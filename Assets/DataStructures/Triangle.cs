﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Triangle {
    public Vertex v1;
    public Vertex v2;
    public Vertex v3;
    public List<Edge> Edges;
    private Vector2 _center = Vector2.positiveInfinity;
    public float Radius { get; private set;}
    
    public Triangle(Vertex v1, Vertex v2, Vertex v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        if (!IsTriangleClockwise(v1.position, v2.position, v3.position)) {
            ChangeOrientation();
        }
        Edges = new List<Edge>() {
            new Edge(v1, v2),
            new Edge(v2, v3),
            new Edge(v3, v1)
        };
        Radius = float.PositiveInfinity;
        _center = Vector2.positiveInfinity;
        GetCircumscribedCircleCenter();
    }

    public Triangle(Vector3 v1, Vector3 v2, Vector3 v3) {
        SetVertices(v1, v2, v3);
    }

    public void SetVertices(Vector3 v1, Vector3 v2, Vector3 v3) {
        this.v1 = new Vertex(v1);
        this.v2 = new Vertex(v2);
        this.v3 = new Vertex(v3);
        if (!IsTriangleClockwise(v1, v2, v3)) {
            ChangeOrientation();
        }
        Edges = new List<Edge>() {
            new Edge(v1, v2),
            new Edge(v2, v3),
            new Edge(v3, v1)
        };
        Radius = float.PositiveInfinity;
        _center = Vector2.positiveInfinity;
        GetCircumscribedCircleCenter();
    }

    public void ChangeOrientation() {
        Vertex temp = this.v1;

        this.v1 = this.v2;

        this.v2 = temp;
    }
    
    public static bool IsTriangleClockwise(Vector2 p1, Vector2 p2, Vector2 p3) {
        bool isClockWise = true;

        float determinant = p1.x * p2.y + p3.x * p1.y + p2.x * p3.y - p1.x * p3.y - p3.x * p2.y - p2.x * p1.y;

        if (determinant > 0f) {
            isClockWise = false;
        }

        return isClockWise;
    }

    //private float GetSurface() {
    //    var segA = new Vector2(v2.position.x - v1.position.x, v2.position.y - v1.position.y);
    //    var segB = new Vector2(v3.position.x - v1.position.x, v3.position.y - v1.position.y);

    //    var centerA = new Vector2((v2.position.x + v1.position.x)/2, (v2.position.y - v1.position.y)/2);
    //    var centerB = new Vector2((v3.position.x - v1.position.x)/2, (v3.position.y - v1.position.y)/2);

    //    var medAB = new Vector2(segA.x, Mathf.Pow(segA.x, 2) / segA.y);

    //    var a = new Vector2(v3.position.x - v2.position.x, v3.position.y - v3.position.y);
    //    var alpha = Vector2.Angle(new Vector2(v2.position.x - v1.position.x, v2.position.y - v1.position.y), new Vector2(v3.position.x - v1.position.x, v3.position.y - v1.position.y));

    //    var R = a * Mathf.Sin(alpha) / 2;
    //    return 0;
    //}

    public Vector2 GetCircumscribedCircleCenter() {
        if (float.IsPositiveInfinity(_center.x)) {
            Vector2 A = v1.position;//new Vector2(5, 3); //
            Vector2 B = v2.position;//new Vector2(1, -3); //
            Vector2 C = v3.position;//new Vector2(-5, 2); //

            var AB = new Vector2(B.x - A.x, B.y - A.y);
            var AC = new Vector2(C.x - A.x, C.y - A.y);

            var ABnormal = new Vector3(AB.y, -AB.x);
                //AB.x,
                //AB.y == 0 ? 0 : AB.x * AB.x / -AB.y
                //);

            var ACnormal = new Vector3(AC.y, -AC.x);
                //AC.x,
                //AC.y == 0 ? 0 : AC.x * AC.x / -AC.y
                //);


            var centerAB = new Vector2((B.x + A.x) / 2, (B.y + A.y) / 2);
            var centerAC = new Vector2((C.x + A.x) / 2, (C.y + A.y) / 2);

           
            //Get the reduced equation of the normal of the edges
            var a = -ABnormal.y / -ABnormal.x;
            var b = -a * centerAB.x + centerAB.y;

            var a2 = -ACnormal.y / -ACnormal.x;
            var b2 = -a2 * centerAC.x + centerAC.y;
            

            if (ABnormal.x == 0) {
                _center = new Vector2(centerAB.x, a2 * centerAB.x + b2);
                //Deduce the radius of the cercle
                var x = A.x - _center.x;
                var y = A.y - _center.y;
                Radius = Mathf.Sqrt(x * x + y * y);

            } else if (ACnormal.x == 0) {
                _center = new Vector2(centerAC.x, a * centerAC.x + b);
                //Deduce the radius of the cercle
                var x = A.x - _center.x;
                var y = A.y - _center.y;
                Radius = Mathf.Sqrt(x * x + y * y);

            } else {
                //Compute the center of the cercle
                //Compute x of the intersection between both normals
                var intersectionX = (b2 - b) / (a - a2);

                //Compute y of the intersection between both normals
                _center = new Vector2(intersectionX, a * intersectionX + b);

                //Deduce the radius of the cercle
                var x = A.x - centerAC.x;
                var y = A.y - centerAC.y;
                Radius = Mathf.Sqrt(x * x + y * y);
            }
        }
        return _center;
    }

    //public float GetRadius() {
    //    if (_radius == float.PositiveInfinity) {
    //        var center = GetCenter();
    //        _radius = Mathf.Sqrt(Mathf.Pow(center.y - v1.position.y, 2) + Mathf.Pow(center.x - v1.position.x, 2));
    //    }
    //    return _radius;
    //}

    public float GetDistance(Vector2 p1, Vector2 center) {
        return Mathf.Sqrt(Mathf.Pow(center.y - p1.y, 2) + Mathf.Pow(center.x - p1.x, 2));
    }

    public bool IsDelauney(Triangle t2) {
       
        List<Vector3> points = new List<Vector3>() {
            t2.v1.position,
            t2.v2.position,
            t2.v3.position
        };

        List<Vector3> points2 = new List<Vector3>() {
            v1.position,
            v2.position,
            v3.position
        };

        //Debug.Log("Before");
        //Debug.Log("List1: " + points[0] + " : "+ points[1] + " : " + points[2]);
        //Debug.Log("List2: " + points2[0] + " : " + points2[1] + " : " + points2[2]);

        points = ( from p in points
                   where 
                   !(
                   v1.position.x == p.x && v1.position.y == p.y ||
                   v2.position.x == p.x && v2.position.y == p.y ||
                   v3.position.x == p.x && v3.position.y == p.y )
                   select p).ToList();

       points2 = (from p in points2
                  where
                  !(
                   t2.v1.position.x == p.x && t2.v1.position.y == p.y ||
                   t2.v2.position.x == p.x && t2.v2.position.y == p.y ||
                   t2.v3.position.x == p.x && t2.v3.position.y == p.y)
                  select p).ToList();

        //Debug.Log("After");
        //foreach (var vector3 in points) {Debug.Log("List1: " + vector3); }
        //foreach (var vector3 in points2) { Debug.Log("List2: " + vector3); }

        var isDelaunay = true;
        
        //Debug.Log(GetCircumscribedCircleCenter());
        if (points.Count == 1 && points2.Count == 1) {
            for (int i = 0; i < points.Count; i++) {
                //Debug.Log("Center: " + GetCircumscribedCircleCenter());
                //Debug.Log(GetDistance(points[i], GetCircumscribedCircleCenter()) + " < " + GetDistance(points2[0], GetCircumscribedCircleCenter()));
                if (GetDistance(points[i], GetCircumscribedCircleCenter()) < GetDistance(points2[0], GetCircumscribedCircleCenter())) {
                    isDelaunay = false;
                }
            }
        }
        return isDelaunay;
    }
}
    