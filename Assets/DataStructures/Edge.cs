﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge {
    public Vertex v1;
    public Vertex v2;

    public Triangle[] Triangles;

    public bool AddTriangle(Triangle t) {
        if (Triangles[0] == null) {
            Triangles[0] = t;
            return true;
        }
        if (Triangles[1] == null) {
            Triangles[1] = t;
            return true;
        }
        return false;
    }

    public Edge(Vertex v1, Vertex v2) {
        this.v1 = v1;
        this.v2 = v2;
        Triangles = new Triangle[2];
    }

    public Edge(Vertex v1, Vertex v2, Triangle t) {
        this.v1 = v1;
        this.v2 = v2;
        Triangles = new Triangle[2];
        AddTriangle(t);
    }

    public Edge(Vector3 v1, Vector3 v2) {
        this.v1 = new Vertex(v1);
        this.v2 = new Vertex(v2);
        Triangles = new Triangle[2];
    }

    public Edge(Vector3 v1, Vector3 v2, Triangle t) {
        this.v1 = new Vertex(v1);
        this.v2 = new Vertex(v2);
        Triangles = new Triangle[2];
        AddTriangle(t);
    }
    
    public Edge(Vector3 v1, Vector3 v2, Triangle t1, Triangle t2) {
        this.v1 = new Vertex(v1);
        this.v2 = new Vertex(v2);
        Triangles = new Triangle[2];
        AddTriangle(t1);
        AddTriangle(t2);
    }



    //public void FlipEdge() {
    //    Vertex temp = v1;

    //    v1 = v2;

    //    v2 = temp;
    //}

    public Vector2 GetMiddlePoint() {
        return (v1.position + v2.position) / 2f;
    }
}