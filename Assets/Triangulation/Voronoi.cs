﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Voronoi {

    public Delaunay D;
    public List<Edge> Edges;
    
    public void ComputeVoronoi(Delaunay d) {
        D = d;
        Edges = new List<Edge>();

        foreach (var edge in D.T.Edges) {
            if (edge.Triangles[1] == null) {
                var _D = edge.Triangles[0].GetCircumscribedCircleCenter();

                var Vertices = new List<Vector3> {
                    edge.Triangles[0].v1.position,
                    edge.Triangles[0].v2.position,
                    edge.Triangles[0].v3.position
                };

                var V = Vertices[0];
                foreach (var v in Vertices) {
                    if ((v.x != edge.v1.position.x || v.y != edge.v1.position.y) && (v.x != edge.v2.position.x || v.y != edge.v2.position.y)) {
                        V = v;
                    }
                }
                var v1 = new Vector2(edge.v1.position.x - V.x, edge.v1.position.y - V.y);
                var v2 = new Vector2(edge.v2.position.x - V.x, edge.v2.position.y - V.y);
                var angle = Vector3.Dot(v1, v2);
                
                var CM = edge.Triangles[0].GetCircumscribedCircleCenter() - edge.GetMiddlePoint();
                var E = new Edge(edge.Triangles[0].GetCircumscribedCircleCenter(), edge.Triangles[0].GetCircumscribedCircleCenter() - CM*100);
                if (angle < 0) {
                    E = new Edge(edge.Triangles[0].GetCircumscribedCircleCenter(), edge.Triangles[0].GetCircumscribedCircleCenter() + CM*100);
                }
                Edges.Add(E);
            } else {
                var e = new Edge(edge.Triangles[0].GetCircumscribedCircleCenter(), edge.Triangles[1].GetCircumscribedCircleCenter());
                Edges.Add(new Edge(edge.Triangles[0].GetCircumscribedCircleCenter(), edge.Triangles[1].GetCircumscribedCircleCenter()));
            }
        }
    }
}
