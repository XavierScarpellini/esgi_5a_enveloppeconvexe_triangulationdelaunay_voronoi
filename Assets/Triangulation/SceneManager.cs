﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SceneManager : MonoBehaviour {

    private Incrementale2D T;
    private Delaunay D;
    private Voronoi V;
    public bool showIncremental = true;
    public bool showDelauneyFlipping = true;
    public bool showVoronoi = true;
    public bool showPointsCoordinates = true;
    public bool showCenters = true;
    public bool showEdges = true;
    public bool showTrianglePerEdges = true;


    [Range(4, 20)]
    public int numberOfPoints = 5;



    //private List<Transform> Points = new List<Transform>();

    void Start() {
        var points = new Vector2[] {
            new Vector2(2, 0),
            new Vector2(0, 4),
            new Vector2(4, 4),
            new Vector2(1, -1),
            new Vector2(4, -3),
            new Vector2(-2, 4),
        };

        T = new Incrementale2D(points);
        T.TriangulatePoints();
        
        var c = new Incrementale2D(points);
        c.TriangulatePoints();
        D = new Delaunay();
        D.FlipEdge(c);

        V = new Voronoi();
        V.ComputeVoronoi(D);
    }

    private Vector2 center2;
    private float v2;

    void FixedUpdate() {
        Incrementale();
    }

    void Update() {
        if (Input.anyKeyDown) {
            if (showTrianglePerEdges) {
                index++;
            } else {
                D.T.Points = new List<Vertex>();
                T.Points = new List<Vertex>();
                for (int i = 0; i < numberOfPoints; i++) {
                    var p = new Vertex(new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), 0));
                    T.Points.Add(p);
                    D.T.Points.Add(p);
                }

                T.TriangulatePoints();
                D.T.TriangulatePoints();
                D.FlipEdge(D.T);
                V = new Voronoi();
                V.ComputeVoronoi(D);
            }
        }
    }

    void OnDrawGizmos() {
        if (showCenters && showTrianglePerEdges && D != null && D.T != null && D.T.Triangles != null) {
            Gizmos.DrawWireSphere(D.T.Edges[index].Triangles[0].GetCircumscribedCircleCenter(), D.T.Edges[index].Triangles[0].Radius);
            if (D.T.Edges[index].Triangles[1] != null) {
                Gizmos.DrawWireSphere(D.T.Edges[index].Triangles[1].GetCircumscribedCircleCenter(), D.T.Edges[index].Triangles[1].Radius);
            }
        }
    }

    GUIStyle s = new GUIStyle();
    void OnGUI() {
        if (T != null && T.Triangles != null) {
            if (showPointsCoordinates) {
                var triangles = T.Points;
                for (var i = 0; i < triangles.Count; i++) {
                    Handles.Label(triangles[i].position, triangles[i].position.ToString());
                    Handles.Label(triangles[i].position, triangles[i].position.ToString());
                    Handles.Label(triangles[i].position, triangles[i].position.ToString());
                }
            }

            if (showCenters) {
                s.normal.textColor = Color.yellow;
                foreach (var triangle in D.T.Triangles) {
                    Handles.Label(triangle.GetCircumscribedCircleCenter(), triangle.GetCircumscribedCircleCenter().ToString(), s);
                }
            }
        }
    }

    private int index = 0;
    public void TriangleByTriangle() {
        if (Input.anyKeyDown) {
            index = index + 1 < T.Triangles.Count ? index + 1 : 0;
        }
        var t = T.Triangles[index];
        Debug.DrawLine(t.v1.position, t.v2.position, Color.green);
        Debug.DrawLine(t.v2.position, t.v3.position, Color.green);
        Debug.DrawLine(t.v3.position, t.v1.position, Color.green);
    }

    public void Incrementale() {
        if (showEdges && D.T != null && D.T.Edges != null) {
            foreach (var edge in T.Edges) {
                var c = Color.blue; // new Color(triangle.v1.position.x, triangle.v1.position.y, triangle.v1.position.z);
                Debug.DrawLine(edge.v1.position, edge.v2.position, c);
            }
            foreach (var edge in D.T.Edges) {
                var c = Color.yellow; // new Color(triangle.v1.position.x, triangle.v1.position.y, triangle.v1.position.z);
                Debug.DrawLine(edge.v1.position, edge.v2.position, c);
            }
        }

        if (showIncremental && T != null && T.Triangles != null) {
            foreach (var triangle in T.Triangles) {
                var c = Color.red; // new Color(triangle.v1.position.x, triangle.v1.position.y, triangle.v1.position.z);
                Debug.DrawLine(triangle.v1.position, triangle.v2.position, c);
                Debug.DrawLine(triangle.v2.position, triangle.v3.position, c);
                Debug.DrawLine(triangle.v3.position, triangle.v1.position, c);
            }
        }
        if (showCenters && D != null && D.T != null && D.T.Triangles != null) {
            foreach (var triangle in D.T.Triangles) {
                var c = Color.yellow;
                Debug.DrawLine(triangle.GetCircumscribedCircleCenter() + Vector2.down * 0.1f, triangle.GetCircumscribedCircleCenter() + Vector2.up * 0.1f, c);
                Debug.DrawLine(triangle.GetCircumscribedCircleCenter() + Vector2.left * 0.1f, triangle.GetCircumscribedCircleCenter() + Vector2.right * 0.1f, c);
            }
        }
        if (showDelauneyFlipping && D != null && D.T != null && D.T.Triangles != null) {
            var triangles = D.T.Triangles;
            for (var i = 0; i < triangles.Count; i++) {
                Debug.DrawLine(triangles[i].v1.position, triangles[i].v2.position, Color.green);
                Debug.DrawLine(triangles[i].v2.position, triangles[i].v3.position, Color.green);
                Debug.DrawLine(triangles[i].v3.position, triangles[i].v1.position, Color.green);
            }
        }

        if (showVoronoi && V != null && V.Edges != null) {
            var Edges = V.Edges;
            for (var i = 0; i < Edges.Count; i++) {
                Debug.DrawLine(Edges[i].v1.position, Edges[i].v2.position, Color.magenta);
            }
        }

        var edges = D.T.Edges;
        if (showTrianglePerEdges && D != null && D.T != null && D.T.Triangles != null) {
            foreach (var edge in edges) {
                Debug.DrawLine(edge.v1.position, edge.v2.position, Color.blue);
            }

            if (index >= edges.Count) {
                index = 0;
            }

            if (edges[index].Triangles[0] != null) {
                Debug.DrawLine(edges[index].Triangles[0].v1.position, edges[index].Triangles[0].v2.position, Color.green);
                Debug.DrawLine(edges[index].Triangles[0].v2.position, edges[index].Triangles[0].v3.position, Color.green);
                Debug.DrawLine(edges[index].Triangles[0].v3.position, edges[index].Triangles[0].v1.position, Color.green);
                Debug.DrawLine(edges[index].v1.position, edges[index].v2.position, Color.red);
            }
            if (edges[index].Triangles[1] != null) {
                Debug.DrawLine(edges[index].Triangles[1].v1.position, edges[index].Triangles[1].v2.position, Color.green);
                Debug.DrawLine(edges[index].Triangles[1].v2.position, edges[index].Triangles[1].v3.position, Color.green);
                Debug.DrawLine(edges[index].Triangles[1].v3.position, edges[index].Triangles[1].v1.position, Color.green);
                Debug.DrawLine(edges[index].v1.position, edges[index].v2.position, Color.red);
            }
        }

    }
}