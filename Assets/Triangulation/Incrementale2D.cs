﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Incrementale2D {
    
    #region Global Variables
    
    public int NumberOfPoints;
    public List<Vertex> Points;
    public List<Triangle> Triangles;
    public List<Edge> Edges;

    #endregion


    public Incrementale2D(Vector2[] points) {
        NumberOfPoints = points.Length;
        Points = new List<Vertex>();
        Triangles = new List<Triangle>();
        Edges = new List<Edge>();


        for (int i = 0; i < NumberOfPoints; i++) {
            Points.Add(new Vertex(points[i]));
        }
    }
    
    public Incrementale2D(int nbPoints) {
        NumberOfPoints = nbPoints;
        Points = new List<Vertex>();
        Triangles = new List<Triangle>();
        Edges = new List<Edge>();


        for (int i = 0; i < NumberOfPoints; i++) {
            Points.Add(new Vertex(new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), 0)));
        }
    }
    
    public void TriangulatePoints() {
        Triangles.Clear();
        Edges.Clear();

        //Tri par x puis par y
        var points = (from point in Points
                    orderby point.position.x, point.position.y
                    select point).ToList();
        
        //Crée un premier triangle
        Triangle newTriangle = new Triangle(points[0].position, points[1].position, points[2].position);
        Triangles.Add(newTriangle);
        
        //Crée les aretes de ce triangle
        Edges.Add(new Edge(newTriangle.v1, newTriangle.v2, newTriangle));
        Edges.Add(new Edge(newTriangle.v3, newTriangle.v1, newTriangle));
        Edges.Add(new Edge(newTriangle.v2, newTriangle.v3, newTriangle));

        //Pour chaque puis non utilisé ...
        for (int k = 3; k < points.Count; k++) {
            var newEdges = new List<Edge>();

            //... parcourir les edges existants ...
            for (int q = 0; q < Edges.Count; q++) {
                var currentEdge = Edges[q];
                
                var edgeMidToCurrentPoint = new Edge(points[k].position, currentEdge.GetMiddlePoint());
                
                var canSeeEdge = true;
                //... parcourir les edges existants ...
                for (var j = 0; j < Edges.Count; j++) {
                    if (j == q) {
                        continue;
                    }
                    
                    //... et tester si la nouvelle aretes coupe une deja existante
                    if (AreEdgesIntersecting(edgeMidToCurrentPoint, Edges[j])) {
                        canSeeEdge = false;

                        break;
                    }
                }
                
                if (canSeeEdge) {

                    var t = new Triangle(currentEdge.v1, new Vertex(points[k].position), currentEdge.v2);
                    Triangles.Add(t);
                    
                    var edgeToPoint1 = new Edge(currentEdge.v1, new Vertex(points[k].position), t);
                    var edgeToPoint2 = new Edge(currentEdge.v2, new Vertex(points[k].position), t);
                    currentEdge.AddTriangle(t);

                    newEdges.Add(edgeToPoint2);
                    newEdges.Add(edgeToPoint1);
                }
            }
            
            for (int j = 0; j < newEdges.Count; j++) {
                Edges.Add(newEdges[j]);
                //Debug.Log(newEdges[j].Triangles[0] == null);
            }
        }
    }

    private bool AreEdgesIntersecting(Edge edge1, Edge edge2) {
        var p1 = edge1.v1.position;
        var p2 = edge1.v2.position;

        var p3 = edge2.v1.position;
        var p4 = edge2.v2.position;

        bool isIntersecting = false;

        float denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);

        if (denominator != 0f) {
            float u_a = ( ( p4.x - p3.x ) * ( p1.y - p3.y ) - ( p4.y - p3.y ) * ( p1.x - p3.x ) ) / denominator;
            float u_b = ( ( p2.x - p1.x ) * ( p1.y - p3.y ) - ( p2.y - p1.y ) * ( p1.x - p3.x ) ) / denominator;
            
            if (u_a >= 0f && u_a <= 1f && u_b >= 0f && u_b <= 1f) {
                isIntersecting = true;
            }
        }

        return isIntersecting;
    }
}
