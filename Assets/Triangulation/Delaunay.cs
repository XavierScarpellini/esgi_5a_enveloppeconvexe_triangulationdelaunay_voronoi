﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Delaunay {
    
    public Incrementale2D T;
    
    public bool IsDelauney(Triangle t1, Triangle t2) {
        return t1.IsDelauney(t2) && t2.IsDelauney(t1);
    }

    public void FlipEdge(Incrementale2D t) {
        T = t;
        var fliped = 1;
        var count = 0;
        while (fliped > 0 && count < 1) {
            fliped = 0;
            for (int i = 0; i < T.Triangles.Count - 1; i++) {
                if (!IsDelauney(T.Triangles[i], T.Triangles[i + 1])) {

                    //Debug.Log("------Flip------");
                    //Debug.Log(T.Triangles[i].v1.position + " : " + T.Triangles[i].v2.position + " + " + T.Triangles[i].v3.position);
                    //Debug.Log(T.Triangles[i + 1].v1.position + " : " + T.Triangles[i + 1].v2.position + " + " + T.Triangles[i + 1].v3.position);

                    List<Vector3> pointsT1 = new List<Vector3>() {
                        T.Triangles[i].v1.position,
                        T.Triangles[i].v2.position,
                        T.Triangles[i].v3.position
                    };
                    List<Vector3> pointsT2 = new List<Vector3>() {
                        T.Triangles[i].v1.position,
                        T.Triangles[i].v2.position,
                        T.Triangles[i].v3.position,
                        T.Triangles[i+1].v1.position,
                        T.Triangles[i+1].v2.position,
                        T.Triangles[i+1].v3.position
                    };

                    var oldEdge = (from p in pointsT1
                        where
                        T.Triangles[i + 1].v1.position.x == p.x && T.Triangles[i + 1].v1.position.y == p.y ||
                        T.Triangles[i + 1].v2.position.x == p.x && T.Triangles[i + 1].v2.position.y == p.y ||
                        T.Triangles[i + 1].v3.position.x == p.x && T.Triangles[i + 1].v3.position.y == p.y
                        select p).ToArray();

                    var newEdge = (from p in pointsT2
                        where
                        !(oldEdge[0].x == p.x && oldEdge[0].y == p.y ||
                          oldEdge[1].x == p.x && oldEdge[1].y == p.y)
                        select p).ToArray();
                    
                    T.Triangles[i].SetVertices(newEdge[0], newEdge[1], oldEdge[0]);
                    T.Triangles[i + 1].SetVertices(newEdge[0], newEdge[1], oldEdge[1]);

                    var oldEdgeIndex = T.Edges.FindIndex(x => x.v1.position == oldEdge[0] && x.v2.position == oldEdge[1] || x.v1.position == oldEdge[1] && x.v2.position == oldEdge[0]);
                    T.Edges[oldEdgeIndex] = new Edge(newEdge[0], newEdge[1]);

                    fliped++;
                }
            }
            T.Edges = new List<Edge>();
            foreach (var triangle in T.Triangles) {
                foreach (var tedge in triangle.Edges) {
                    var index = T.Edges.FindIndex(e =>
                        e.v1.position == tedge.v1.position && e.v2.position == tedge.v2.position ||
                        e.v2.position == tedge.v1.position && e.v1.position == tedge.v2.position);

                    if (index == -1) {
                        tedge.AddTriangle(triangle);
                        T.Edges.Add(tedge);
                    } else {
                        T.Edges[index].AddTriangle(triangle);
                    }
                }
            }
            count++;
        }
    }
}
