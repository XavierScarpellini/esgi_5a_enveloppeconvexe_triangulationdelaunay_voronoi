﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class GrahamScan : MonoBehaviour {

    const int TurnLeft = 1;
    private List<Vector2> _result;
    private List<Vector2> _listPoints = null;
    [Range(3, 20)]
    public int nbPoints = 3;

    public bool randomPoints;
    public bool draw;

    public void Start()
    {
       
    }

    void Update()
    {
        if (draw == true)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (_listPoints == null)
                {
                    _listPoints = new List<Vector2>();
                }
                Vector3 mousePosition = Input.mousePosition;
                var screenPoint = Input.mousePosition;
                screenPoint.z = 10.0f;
                _listPoints.Add(Camera.main.ScreenToWorldPoint(screenPoint));

                if(_listPoints.Count >=3)
                    ConvexHull(_listPoints);
            }
        }
        

        if (randomPoints == true && _listPoints == null)
        {
            _listPoints = new List<Vector2>();
            for (int i2 = 0; i2 < nbPoints; i2++)
            {
                _listPoints.Add(new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), 0));
            }
            ConvexHull(_listPoints);
        }

        if(_listPoints!= null)
        {
            for (int k = 0; k < _listPoints.Count; k++)
            {
                Debug.DrawLine(_listPoints[k] + Vector2.left*0.1f, _listPoints[k] + Vector2.right * 0.1f);
                Debug.DrawLine(_listPoints[k] + Vector2.up * 0.1f, _listPoints[k] + Vector2.down * 0.1f);
            }
        }
        
        if (_result != null && _result.Count > 2) {
            for (int i = 1; i < _result.Count; i++) {
                Debug.DrawLine(_result[i - 1], _result[i], Color.Lerp(Color.red, Color.green, (float)i/_result.Count));

            }
            Debug.DrawLine(_result[0], _result[_result.Count-1], Color.green);

        }
    }

    void OnGUI()
    {
        if (_listPoints != null)
        {
            foreach (Vector3 point in _listPoints)
            {
                Handles.Label(point, "(" + point.x.ToString("#.##") + ";" + point.y.ToString("#.##") + ")");

            }
        }
    }

    private int Turn(Vector2 p, Vector2 q, Vector2 r) {
        return ((q.x - p.x) * (r.y - p.y) - (r.x - p.x) * (q.y - p.y)).CompareTo(0);
    }

    private void KeepLeft(List<Vector2> hull, Vector2 r) {
        while (hull.Count > 1 && Turn(hull[hull.Count - 2], hull[hull.Count - 1], r) != TurnLeft) {
            hull.RemoveAt(hull.Count - 1);
        }
        if (hull.Count == 0 || hull[hull.Count - 1] != r) {
            hull.Add(r);
        }
    }

    private double GetAngle(Vector2 p1, Vector2 p2) {
        return Math.Atan2(p2.y - p1.y, p2.x - p1.x) * 180.0 / Math.PI;
    }

    private List<Vector2> MergeSort(Vector2 p0, List<Vector2> arrVector2) {
        if (arrVector2.Count == 1) {
            return arrVector2;
        }
        var arrSortedInt = new List<Vector2>();
        var middle = arrVector2.Count / 2;
        var leftptr = 0;
        var rightptr = 0;
        var leftArray = arrVector2.GetRange(0, middle);
        var rightArray = arrVector2.GetRange(middle, arrVector2.Count - middle);

        leftArray = MergeSort(p0, leftArray);
        rightArray = MergeSort(p0, rightArray);
        for (var i = 0; i < leftArray.Count + rightArray.Count; i++) {
            if (leftptr == leftArray.Count) {
                arrSortedInt.Add(rightArray[rightptr]);
                rightptr++;
            } else if (rightptr == rightArray.Count) {
                arrSortedInt.Add(leftArray[leftptr]);
                leftptr++;
            } else if (GetAngle(p0, leftArray[leftptr]) < GetAngle(p0, rightArray[rightptr])) {
                arrSortedInt.Add(leftArray[leftptr]);
                leftptr++;
            } else {
                arrSortedInt.Add(rightArray[rightptr]);
                rightptr++;
            }
        }
        return arrSortedInt;
    }

    private void ConvexHull(List<Vector2> vector2S) {

        var p0 = Vector2.positiveInfinity;
        foreach (var value in vector2S) {
            if (p0 == Vector2.positiveInfinity) {
                p0 = value;
            } else {
                if (p0.y > value.y) {
                    p0 = value;
                }
            }
        }
        var order = vector2S.Where(value => p0 != value).ToList();

        order = MergeSort(p0, order);
        _result = new List<Vector2> {
            p0,
            order[0],
            order[1]
        };
        order.RemoveAt(0);
        order.RemoveAt(0);

        foreach (Vector2 value in order) {
            KeepLeft(_result, value);
        }
    }
}


