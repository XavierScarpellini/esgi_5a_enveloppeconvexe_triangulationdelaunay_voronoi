using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;


public class GeneratePoints : MonoBehaviour
{

    [Range(3, 20)]
    public int nbPoints = 3;

    public bool randomPoints;
    public bool draw;


    private List<Vector2> Points;
    private int i0;
    private float xmin;
    private float ymin;


    private Vector2 v = new Vector2(0, -1);
    private List<Vector2> Polygon = new List<Vector2>();

    int i;

    // Use this for initialization
    void Start()
    {
    }


    void Update()
    {

        if (draw == true)
        {
            if (Input.GetMouseButtonUp(0))
            {
                if(Points == null)
                {
                    Points = new List<Vector2>();
                }
                Vector3 mousePosition = Input.mousePosition; 
                var screenPoint = Input.mousePosition;
                screenPoint.z = 10.0f;
                Points.Add(Camera.main.ScreenToWorldPoint(screenPoint));
                if(Points.Count >= 3)
                    generatePolygon();
            }
        }


        if(randomPoints == true && Points == null)
        {
            Points = new List<Vector2>();
            for (int i2 = 0; i2 < nbPoints; i2++)
            {
                Points.Add(new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), 0));
            }

            generatePolygon();
        }

        if(Polygon != null && Polygon.Count >= 3)
        {
            for (int k = 0; k < Polygon.Count - 1; k++)
            {
                Debug.DrawLine(Polygon[k], Polygon[k + 1]);
            }

            Debug.DrawLine(Polygon[Polygon.Count-1], Polygon[0]);
        }

        
    }

    void OnGUI()
    {
        if(Points != null)
        {
            foreach (Vector3 point in Points)
            {
                Handles.Label(point, "(" + point.x.ToString("#.##") + ";" + point.y.ToString("#.##") + ")");
            }
        }
        
    }

    public void generatePolygon()
    {
        Polygon.Clear();
        i0 = 0;
        xmin = Points[i0].x;
        ymin = Points[i0].y;


        for (int tempi = 1; tempi < Points.Count; tempi++)
        {
            if ((Points[tempi].x < xmin) || ((Points[tempi] == Points[i0]) && (Points[tempi].y < ymin)))
            {
                i0 = tempi;
                xmin = Points[i0].x;
                ymin = Points[i0].y;
            }
        }

        i = i0;
        int j = 0;
        float minAngle = 0.0f;
        float lMax = 0.0f;
        int inew;

        do
        {
            Polygon.Add(Points[i]);
            if (i == 0)
            {
                j = 1;
            }
            else {
                j = 0;
            }

            Vector2 vector = Points[j] - Points[i];

            minAngle = Vector2.Angle(v, vector);
            lMax = Vector2.SqrMagnitude(vector);
            inew = j;

            for (j = inew + 1; j < Points.Count; j++)
            {
                if (j != i)
                {
                    vector = Points[j] - Points[i];
                    float angle = Vector2.Angle(v, vector);
                    if ((minAngle > angle) || (minAngle == angle && lMax < Vector2.SqrMagnitude(vector)))
                    {
                        minAngle = angle;
                        lMax = Vector2.SqrMagnitude(vector);
                        inew = j;
                    }
                }
            }

            v = Points[inew] - Points[i];
            i = inew;

        } while (i != i0);
        
    }
}